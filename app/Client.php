<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'name', 'cities_id',
    ];

    protected $table = 'clients';

    public function cities()
    {
        return $this->belongsTo('App\City');
    }
}

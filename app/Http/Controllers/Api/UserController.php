<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;

    public function login(Request $request)
    {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

            $user = Auth::user();
            $userDetails = User::find($user->id);
            $success['token'] =  $user->createToken('estounaprueba')->accessToken;
            $success['email'] =  $user->email;
            $success['id'] =  $user->id;

            return response()->json(['success' => $success], $this->successStatus);
        } else{
            return response()->json(['error'=>'Unauthorised'], $this->errorStatus);
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->first()], $this->errorStatus);
        }

        try {
            DB::connection()->getPdo()->beginTransaction();
                User::where('id',$request->id)->update(['name'=>$request->name]);
                $success['name'] =  $request->name;
            DB::connection()->getPdo()->commit();
            return response()->json(['success' => $success], $this->successStatus);

        } catch (\PDOException $e) {
            DB::connection()->getPdo()->rollBack();
            return response()->json(['error'=> $e], $this->errorStatus);
        }

    }
}

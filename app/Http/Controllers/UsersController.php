<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $users  = User::all();
        return view('users.index')->with('users', $users);
    }

    public function add(Request $request)
    {
        return view('users.add');
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name',
            'email' => 'required|unique:users|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return Redirect::route('addusers')->withErrors(['error' => "Data don't completed"]);
        }

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return Redirect::route('users');
    }

    public function edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        $user = User::find($request->id);
        return view('users.edit')->with('user', $user);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return Redirect::route('addusers')->withErrors(['error' => "Data don't completed"]);
        }

        User::where('id',$request->id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return Redirect::route('users');
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        $user = User::find($request->id);
        return view('users.delete')->with('user', $user);
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        $user = User::where('id',$request->id)->delete();
        return Redirect::route('users');
    }
}

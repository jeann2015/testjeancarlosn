<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class CityController extends Controller
{
    public function index(Request $request)
    {
        $cities  = City::all();
        return view('cities.index')->with('cities', $cities);
    }

    public function add(Request $request)
    {
        return view('cities.add');
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::route('addcities')->withErrors(['error' => "Data don't completed"]);
        }

        City::create([
            'name' => $request->name,
        ]);

        return Redirect::route('cities');
    }

    public function edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        $city = City::find($request->id);
        return view('cities.edit')->with('city', $city);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::route('editcities')->withErrors(['error' => "Data don't completed"]);
        }

        City::where('id',$request->id)->update([
            'name' => $request->name,
        ]);

        return Redirect::route('cities');
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        $city = City::find($request->id);
        return view('cities.delete')->with('city', $city);
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'  => 'required',
            'name'  => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::route('deletecities')->withErrors(['error' => "Data don't completed"]);
        }

        City::where('id',$request->id)->delete();

        return Redirect::route('cities');
    }



}

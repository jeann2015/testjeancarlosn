<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\City;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        $clients  = Client::all();
        return view('clients.index')->with('clients', $clients);
    }

    public function add(Request $request)
    {
        $cities  = City::all();
        return view('clients.add')->with('cities', $cities);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'city_id'  => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::route('addclients')->withErrors(['error' => "Data don't completed"]);
        }

        Client::create([
            'name' => $request->name,
            'cities_id' => $request->city_id,
        ]);

        return Redirect::route('clients');
    }

    public function edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        $client = Client::find($request->id);
        $cities  = City::all();
        return view('clients.edit')->with('cities', $cities)->with('client',$client);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'city_id'  => 'required',
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::route('editclients')->withErrors(['error' => "Data don't completed"]);
        }

        Client::where('id',$request->id)->update([
            'name' => $request->name,
            'cities_id' => $request->city_id,
        ]);

        return Redirect::route('clients');
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        $client = Client::find($request->id);
        $cities  = City::all();
        return view('clients.delete')->with('cities', $cities)->with('client',$client);
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::route('deleteclients')->withErrors(['error' => "Data don't completed"]);
        }

        Client::where('id',$request->id)->delete();
        return Redirect::route('clients');
    }
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth'])->group(function () {
    Route::get('/users', 'UsersController@index')->name('users');
    Route::get('/addusers', 'UsersController@add')->name('addusers');
    Route::post('/saveusers', 'UsersController@save')->name('saveusers');
    Route::get('/editusers/{id}', 'UsersController@edit')->name('editusers');
    Route::post('/updateusers', 'UsersController@update')->name('updateusers');
    Route::get('/deleteusers/{id}', 'UsersController@delete')->name('deleteusers');
    Route::post('/destroyusers', 'UsersController@destroy')->name('destroyusers');

    Route::get('/clients', 'ClientController@index')->name('clients');
    Route::get('/addclients', 'ClientController@add')->name('addclients');
    Route::post('/saveclients', 'ClientController@save')->name('saveclients');
    Route::get('/editclients/{id}', 'ClientController@edit')->name('editclients');
    Route::post('/updateclients', 'ClientController@update')->name('updateclients');
    Route::get('/deleteclients/{id}', 'ClientController@delete')->name('deleteclients');
    Route::post('/destroyclients', 'ClientController@destroy')->name('destroyclients');

    Route::get('/cities', 'CityController@index')->name('cities');
    Route::get('/addcities', 'CityController@add')->name('addcities');
    Route::post('/savecities', 'CityController@save')->name('savecities');
    Route::get('/editcities/{id}', 'CityController@edit')->name('editcities');
    Route::post('/updatecities', 'CityController@update')->name('updatecities');
    Route::get('/deletecities/{id}', 'CityController@delete')->name('deletecities');
    Route::post('/destroycities', 'CityController@destroy')->name('destroycities');
});

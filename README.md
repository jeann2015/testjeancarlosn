<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


## Mis perfiles
<a href="https://jeancarlosn.com" target="_blank">Pagina de Jean Carlos</a>

<a href="https://laracasts.com/@jeancarlosn2008@gmail.com" target="_blank">Perfil de Laracast</a>

<a href="https://platzi.com/p/jeancarlosn/" target="_blank">Perfil de Platzi</a>


## Pasos para instalar

1-composer install

2-php artisan migrate

3-php artisan passport:install

4-php artisan voyager:admin your@email.com --create Para crear usuario admin

## Pasos para usar API con Postman

1-Instalar postman

2-Endpoint para login http://localhost:8080/api/login, le dejo el codigo en curl

`curl -X POST \
  http://127.0.0.1:8091/api/login \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 6c7bf6cf-32cf-a8be-948a-a9ff81d0249c' \
  -H 'x-requested-with: XMLHttpRequest' \
  -d '{	
	"email":"email@email.com",
	"password":"password",
	"remember_me":true
}'`

Ejecutar este endpoint, regresa un token para poder hacer el update del usuario

3-Para actualizar el nombre del user, Endpoint para login http://localhost:8080/api/update, le dejo el codigo en curl

`curl -X POST \
  http://127.0.0.1:8091/api/update \
  -H 'authorization: Bearer cadenadetoken' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: f7c358eb-5336-fd44-df26-615a5607416f' \
  -d '{
	"name": "Jose",
	"id":"1"
}'`

@extends('layouts.app')
@section('content')

<div class="container">
        <h3>Cities</h3>
        <button type="button" class="btn btn-success"><i class="bi bi-file-earmark-plus"><a href="{{ route('addcities') }}">Add</a></i></button>
        <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cities as $city)
                    <tr>
                        <td>{{ $city->name }}</td>
                        <td>
                            <a class="btn btn-primary" href="{{ route('editcities', ['id' => $city->id]) }}"><i class="bi bi-vector-pen">Edit</a></i>
                            <a class="btn btn-danger" href="{{ route('deletecities', ['id' => $city->id]) }}"><i class="bi bi-x-circle">Delete</a></i>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </tfoot>
        </table>
</div>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection




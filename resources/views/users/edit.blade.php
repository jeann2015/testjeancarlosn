@extends('layouts.app')
@section('content')

<div class="container">
        <h3>Users</h3>
        <button type="button" class="btn btn-light"><i class="bi bi-arrow-90deg-left"><a href="{{ route('users')}} ">Back</a></i></button>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div>
        @endif
        <form action="{{ route('updateusers') }}" method="post">
        @csrf
        <table class="table" style="width:100%">

                <tr>
                    <td>Name</td>
                    <td>
                        <input type="text" name="name" value="{{$user->name}}" class="form-control" placeholder="Name">
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>
                        <input type="email" name="email" class="form-control" value="{{$user->email}}" placeholder="Email">
                    </td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>
                        <input type="password" name="password" class="form-control" >
                    </td>
                </tr>

            <tr>
                <td colspan="2">
                    <button type="submit" class="btn btn-success"><i class="bi bi-save">Update</i></button>
                    <input type="hidden" name="id" value="{{ $user->id }}">
                </td>
            </tr>
        </tfoot>
        </table>
    </form>
</div>

@endsection

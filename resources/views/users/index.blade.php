@extends('layouts.app')
@section('content')

<div class="container">
        <h3>Users</h3>
        <button type="button" class="btn btn-success"><i class="bi bi-file-earmark-plus"><a href="{{ route('addusers') }}">Add</a></i></button>
        <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <a class="btn btn-primary" href="{{ route('editusers', ['id' => $user->id]) }}"><i class="bi bi-vector-pen">Edit</a></i>
                            <a class="btn btn-danger" href="{{ route('deleteusers', ['id' => $user->id]) }}"><i class="bi bi-x-circle">Delete</a></i>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </tfoot>
        </table>

</div>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection




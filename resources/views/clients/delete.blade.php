@extends('layouts.app')
@section('content')

<div class="container">
        <h3>Clients</h3>
        <button type="button" class="btn btn-light"><i class="bi bi-arrow-90deg-left"><a href="{{ route('clients')}} ">Back</a></i></button>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div>
        @endif
        <form action="{{ route('destroyclients') }}" method="post">
        @csrf
        <table class="table" style="width:100%">

                <tr>
                    <td>Name</td>
                    <td>
                        <input type="text" name="name" value="{{$client->name}}" class="form-control" placeholder="Name">
                    </td>
                </tr>
                <tr>
                    <td>City</td>
                    <td>
                        <select class="form-control" name="city_id" id="city_id">
                            @foreach ($cities as $city)
                                @if($client->city_id == $city->id)
                                    <option selected value="{{ $city->id}}">{{ $city->name }}</option>
                                @else
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </td>
                </tr>
            <tr>
                <td colspan="2">
                    <button type="submit" class="btn btn-danger"><i class="bi bi-x-circle">Delete</i></button>
                    <input type="hidden" name="id" value="{{ $client->id }}">
                </td>
            </tr>
        </tfoot>
        </table>
    </form>
</div>

@endsection

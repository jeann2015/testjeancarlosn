@extends('layouts.app')
@section('content')

<div class="container">
        <h3>Clients</h3>
        <button type="button" class="btn btn-success"><i class="bi bi-file-earmark-plus"><a href="{{ route('addclients') }}">Add</a></i></button>
        <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>City</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($clients as $client)
                    <tr>
                        <td>{{ $client->name }}</td>
                        <td>{{ $client->cities->name }}</td>
                        <td>
                            <a class="btn btn-primary" href="{{ route('editclients', ['id' => $client->id]) }}"><i class="bi bi-vector-pen">Edit</a></i>
                            <a class="btn btn-danger" href="{{ route('deleteclients', ['id' => $client->id]) }}"><i class="bi bi-x-circle">Delete</a></i>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </tfoot>
        </table>

</div>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection



